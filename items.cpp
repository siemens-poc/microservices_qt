#include "items.h"
#include "ui_items.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
#include <iostream>

Items::Items(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Items)
{
    ui->setupUi(this);

    static QString strItems;
    static QStringList Itemlist;
    static QList<int> itemId;
    static QList<QString> ItemName;
    static QList<QString> ItemRevision;
    static QList<QString> ItemCreatedBy;

    manager = new QNetworkAccessManager();

    QObject::connect(manager, &QNetworkAccessManager::finished,this, [=](QNetworkReply *reply)
    {
        if (reply->error())
        {
            qDebug() << reply->errorString();
            return;
        }

        static QString answer = reply->readAll();
        strItems = answer;


    // Either this if you use UTF-8 anywhere
    std::string strItem = answer.toUtf8().constData();
    //std::cout<< utf8_text;


    strItems.remove(QRegExp("\n"));
    strItems.remove(QRegExp("\""));
    strItems.remove(QRegExp("[{]"));
    strItems.remove(QRegExp("[[]"));
    strItems.remove(QRegExp("[\"]"));
    strItems.remove(QRegExp("[ ]"));

    //Split Item Record
    static QStringList list = strItems.split('}');

    for(int i = 0; i<list.length()-1;i++)
    {
        strItems = list.at(i);
        Itemlist.clear();

        Itemlist = strItems.split(',');
        for(int j = 0; j<Itemlist.length();j++)
        {
            strItems = "";
            strItems = Itemlist.at(j);
            strItems.remove("itemid:", Qt::CaseInsensitive);
            strItems.remove("name:", Qt::CaseInsensitive);
            strItems.remove("revision:", Qt::CaseInsensitive);
            strItems.remove("createdby:", Qt::CaseInsensitive);
            if(i == 0)
            {
                if (j == 0)
                {
                    int n = strItems.toInt();
                    itemId.append(n);
                }
                else if (j == 1)
                {
                    ItemName.append(strItems);
                }
                else if (j == 2)
                {
                    ItemRevision.append(strItems);
                }
                else if (j == 3)
                {
                     ItemCreatedBy.append(strItems);
                }
            }
            else
            {
                if (j == 1)
                {
                    int n = strItems.toInt();
                    itemId.append(n);
                }
                else if (j == 2)
                {
                    ItemName.append(strItems);
                }
                else if (j == 3)
                {
                    ItemRevision.append(strItems);
                }
                else if (j == 4)
                {
                     ItemCreatedBy.append(strItems);
                }
            }
        }
    }
    // Create model:
    ItemModel *ptrItemModel = new ItemModel(this);

    // Populate model with data:
    ptrItemModel->populateData(itemId,ItemName,ItemRevision,ItemCreatedBy);

    // Connect model to table view:

    ui->tableView->setModel(ptrItemModel);

    // Make table header visible and display table:
    ui->tableView->horizontalHeader()->setVisible(true);
    ui->tableView->show();
    }
    );
}


Items::~Items()
{
    delete ui;
}


void Items::on_pushButton_clicked()
{
    request.setUrl(QUrl("http://localhost:8002/items"));
    manager->get(request);
}

void Items::managerFinished(QNetworkReply *reply)
{
    if (reply->error())
    {
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readAll();

    //qDebug() << answer;
}

ItemModel::ItemModel(QObject *parent) : QAbstractTableModel(parent)
{
}

// Create a method to populate the model with data:
void ItemModel:: populateData(const QList<int> &Id,const QList<QString> &Name,const QList<QString> &Revision,const QList<QString> &CreatedBy)
{
    im_item_Id.clear();
    im_item_Id= Id;

    im_item_Name.clear();
    im_item_Name = Name;

    im_item_Revision.clear();
    im_item_Revision = Revision;

    im_item_CreatedBy.clear();
    im_item_CreatedBy = CreatedBy;
    return;
}

int ItemModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return im_item_Id.length();
}

int ItemModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 4;
}

QVariant ItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole) {
        return QVariant();
    }
    if (index.column() == 0)
    {
        return im_item_Id[index.row()];
    }
    else if (index.column() == 1)
    {
        return im_item_Name[index.row()];
    }
    else if (index.column() == 2)
    {
        return im_item_Revision[index.row()];
    }
    else if (index.column() == 3)
    {
        return im_item_CreatedBy[index.row()];
    }
    return QVariant();
}

QVariant ItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        if (section == 0)
        {
            return QString("ID");
        }
        else if (section == 1)
        {
            return QString("Name");
        }
        else if (section == 2)
        {
            return QString("Revision");
        }
        else if (section == 3)
        {
            return QString("createdBy");
        }
    }
    return QVariant();
}



