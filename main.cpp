#include "microservices.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MicroServices w;
    w.show();
    return a.exec();
}
