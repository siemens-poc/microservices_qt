#include "microservices.h"
#include "ui_microservices.h"
#include "items.h"
#include "users.h"

MicroServices::MicroServices(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MicroServices)
{
    ui->setupUi(this);
    connect(ui->tabWidget,SIGNAL(currentChanged(int)),this, SLOT(updateSizes(int)));
    ui->tabWidget->addTab(new Items,"Items");
    ui->tabWidget->addTab(new Users,"Users");
}

MicroServices::~MicroServices()
{
    delete ui;
}

void MicroServices::updateSizes(int index)
{
    for(int i=0;i<ui->tabWidget->count();i++)
        if(i!=index)
            ui->tabWidget->widget(i)->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    ui->tabWidget->widget(index)->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    ui->tabWidget->widget(index)->resize(ui->tabWidget->widget(index)->minimumSizeHint());
    ui->tabWidget->widget(index)->adjustSize();
    resize(minimumSizeHint());
    adjustSize();
}
