#ifndef ITEMS_H
#define ITEMS_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QAbstractTableModel>

namespace Ui {
class Items;
}

class ItemModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    ItemModel(QObject *parent = 0);

    void populateData(const QList<int> &Id,const QList<QString> &Name,
                      const QList<QString> &Revision,const QList<QString> &CreatedBy);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

private:
    QList<int> im_item_Id;
    QList<QString> im_item_Name;
    QList<QString> im_item_Revision;
    QList<QString> im_item_CreatedBy;

};
class Items : public QMainWindow
{
    Q_OBJECT
public:
    explicit Items(QWidget *parent = nullptr);
    ~Items();

private slots:
    void managerFinished(QNetworkReply *reply);
    void on_pushButton_clicked();

private:
    Ui::Items *ui;
    QNetworkAccessManager *manager;
    QNetworkRequest request;
};

#endif // MAINWINDOW_H

