#ifndef USERS_H
#define USERS_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QAbstractTableModel>

namespace Ui {
class Users;
}

class UserModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    UserModel(QObject *parent = 0);

    void populateData(const QList<int> &Id,const QList<QString> &Name,
                      const QList<QString> &email,const QList<QString> &Usergroup,const QList<QString> &Role);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

private:
    QList<int> um_user_Id;
    QList<QString> um_user_Name;
    QList<QString> um_email;
    QList<QString> um_usergroup;
     QList<QString> um_role;

};
class Users : public QMainWindow
{
    Q_OBJECT

public:
    explicit Users(QWidget *parent = nullptr);
    ~Users();

private slots:
    void managerFinished(QNetworkReply *reply);
    void on_UserData_clicked();

private:
    Ui::Users *ui;
    QNetworkAccessManager *manager;
      QNetworkRequest request;
};


#endif // MAINWINDOW_H

