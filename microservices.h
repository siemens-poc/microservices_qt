#ifndef MICROSERVICES_H
#define MICROSERVICES_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MicroServices; }
QT_END_NAMESPACE

class MicroServices : public QMainWindow
{
    Q_OBJECT

public:
    MicroServices(QWidget *parent = nullptr);
    ~MicroServices();
    void updateSizes(int index);

private:
    Ui::MicroServices *ui;
};
#endif // MICROSERVICES_H
