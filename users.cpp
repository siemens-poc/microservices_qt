#include "users.h"
#include "ui_users.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
#include <iostream>

Users::Users(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Users)
{
    ui->setupUi(this);
    static QString answer1;
    static QStringList list1;
    static QList<int> userId;
    static QList<QString> UserName;
    static QList<QString> email;
    static QList<QString>Usergroup;
    static QList<QString>role;

    manager = new QNetworkAccessManager();
    QObject::connect(manager, &QNetworkAccessManager::finished,this, [=](QNetworkReply *reply)
    {
        if (reply->error())
        {
            qDebug() << reply->errorString();
            return;
        }

        static QString answer = reply->readAll();
        answer1 = answer;


        answer1.remove(QRegExp("\n"));
        answer1.remove(QRegExp("\""));
        answer1.remove(QRegExp("[{]"));
        answer1.remove(QRegExp("[[]"));
        answer1.remove(QRegExp("[\"]"));
        answer1.remove(QRegExp("[ ]"));

        static QStringList list = answer1.split('}');

        for(int i = 0; i<list.length()-1;i++)
        {
            answer1 = list.at(i);
            //qDebug()<<answer1;
            list1.clear();

            list1 = answer1.split(',');
            for(int j = 0; j<list1.length();j++)
            {
                answer1 = "";

                answer1 = list1.at(j);
                answer1.remove("id:", Qt::CaseInsensitive);

                answer1.remove("name:", Qt::CaseInsensitive);
                answer1.remove("email:", Qt::CaseInsensitive);
                answer1.remove("Usergroup:", Qt::CaseInsensitive);
                answer1.remove("role:", Qt::CaseInsensitive);

                if(i == 0)
                {
                    if (j == 0)
                    {
                        int n = answer1.toInt();
                        userId.append(n);
                        qDebug()<<answer1;
                    }
                    else if (j == 1)
                    {
                        UserName.append(answer1);
                    }
                    else if (j == 2)
                    {
                        email.append(answer1);
                    }
                    else if (j == 3)
                    {
                        Usergroup.append(answer1);
                    }
                    else if (j == 4)
                    {
                        role.append(answer1);
                    }
                }
                else
                {
                    if (j == 1)
                    {
                          int n = answer1.toInt();
                         userId.append(n);
                    }
                    else if (j == 2)
                    {
                        UserName.append(answer1);
                    }
                    else if (j == 3)
                    {
                        email.append(answer1);
                    }
                    else if (j == 4)
                    {
                         Usergroup.append(answer1);
                    }
                    else if (j == 5)
                    {
                        role.append(answer1);
                    }
                }
            }
        }
        // Create model:
        UserModel *PhoneBookModel = new UserModel(this);

        // Populate model with data:
        PhoneBookModel->populateData(userId,UserName,email,Usergroup,role);

        // Connect model to table view:
        ui->tableView->setModel(PhoneBookModel);

        // Make table header visible and display table:
        ui->tableView->horizontalHeader()->setVisible(true);
        ui->tableView->show();
        }
    );
}

Users::~Users()
{
     delete ui;
}

void Users::managerFinished(QNetworkReply *reply)
{
    if (reply->error())
    {
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readAll();

    //qDebug() << answer;
}


UserModel::UserModel(QObject *parent) : QAbstractTableModel(parent)
{
}

// Create a method to populate the model with data:
void UserModel:: populateData(const QList<int> &Id,const QList<QString> &Name,const QList<QString> &Email,const QList<QString> &Usergroup,const QList<QString> &Role)
{
    um_user_Id.clear();
    um_user_Id= Id;

    um_user_Name.clear();
    um_user_Name = Name;

    um_email.clear();
    um_email = Email;

    um_usergroup.clear();
    um_usergroup = Usergroup;

    um_role.clear();
    um_role = Role;
    return;
}

int UserModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return um_user_Id.length();
}

int UserModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 5;
}

QVariant UserModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole)
    {
        return QVariant();
    }
    if (index.column() == 0)
    {
        return um_user_Id[index.row()];
    }
    else if (index.column() == 1)
    {
        return um_user_Name[index.row()];
    }
    else if (index.column() == 2)
    {
        return um_email[index.row()];
    }
    else if (index.column() == 3)
    {
        return um_usergroup[index.row()];
    }
    else if (index.column() == 4)
    {
        return um_role[index.row()];
    }
    return QVariant();
}

QVariant UserModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        if (section == 0)
        {
            return QString("ID");
        }
        else if (section == 1)
        {
            return QString("Name");
        }
        else if (section == 2)
        {
            return QString("Email");
        }
        else if (section == 3)
        {
            return QString("Usergroup");
        }
        else if (section == 4)
        {
            return QString("Role");
        }
    }
    return QVariant();
}


void Users::on_UserData_clicked()
{
    request.setUrl(QUrl("http://localhost:8001/users"));
     manager->get(request);
}

